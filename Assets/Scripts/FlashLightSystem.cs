using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightSystem : MonoBehaviour
{
    [SerializeField] private float lightDecay = .02f;
    [SerializeField] private float angleDecay = .02f;

    [SerializeField] private float minimumAngle = 40f;

    private Light theLight;

    private void Start()
    {
        theLight = GetComponent<Light>();
    }

    private void Update()
    {
        DecreseLightIntensity();
        DecreseLightAngle();
    }

    public void RestoreLightAngle(float restoreAngle)
    {
        theLight.spotAngle = restoreAngle;
    }
    
    public void RestoreLightIntensity(float restoreIntensity)
    {
        theLight.intensity = restoreIntensity;
    }

    private void DecreseLightAngle()
    {
        theLight.intensity -= lightDecay * Time.deltaTime;
    }

    private void DecreseLightIntensity()
    {
        if (theLight.spotAngle > minimumAngle)
        {
            theLight.spotAngle -= angleDecay * Time.deltaTime;
        }
    }
}
