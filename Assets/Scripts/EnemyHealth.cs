using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private float hitPoints = 100f;

    private bool isDead;

    public bool IsDead()
    {
        return isDead;
    }
    
    public void TakeDamage(float damageAmount)
    {
        // Event sent to the rest of the components of the game object
        BroadcastMessage("OnDamageTaken");
        
        hitPoints -= damageAmount;

        if (hitPoints < 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if (isDead)
        {
            return;
        }
        
        isDead = true;
        GetComponent<Animator>().SetBool("Die", true);
    }
}
