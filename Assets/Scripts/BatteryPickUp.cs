using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPickUp : MonoBehaviour
{
    [SerializeField] private float restoreAngle = 55.0f;
    [SerializeField] private float restoreIntensity = 2f;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            return;
        }
        
        other.GetComponentInChildren<FlashLightSystem>().RestoreLightAngle(restoreAngle);
        other.GetComponentInChildren<FlashLightSystem>().RestoreLightIntensity(restoreIntensity);
            
        Destroy(gameObject);
    }
}
