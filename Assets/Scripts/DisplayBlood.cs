using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayBlood : MonoBehaviour
{
    [SerializeField] Canvas damageCanvas;
    [SerializeField] private float impactTime = 0.3f;

    private void Start()
    {
        damageCanvas.enabled = false;
    }

    public void DisplayDamageCanvas()
    {
        StartCoroutine(ShowBlood());
    }

    IEnumerator ShowBlood()
    {
        damageCanvas.enabled = true;
        yield return new WaitForSeconds(impactTime);
        damageCanvas.enabled = false;
    }
}
