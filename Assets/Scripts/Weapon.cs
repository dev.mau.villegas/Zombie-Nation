using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Camera fpsCamera;
    [SerializeField] private float range = 100f;
    [SerializeField] private float damage = 25f;
    [SerializeField] private float shootDelay = 1.0f;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private GameObject hitEffect;
    [SerializeField] private Ammo ammoSlot;
    [SerializeField] private AmmoType ammoType;
    [SerializeField] private TextMeshProUGUI ammoText;

    private bool _canShoot = true;

    private void OnEnable()
    {
        _canShoot = true;
    }

    void Update()
    {
        DisplayAmmo();
        
        if (Input.GetMouseButtonDown(0) && _canShoot)
        {
            StartCoroutine(Shoot());
        }
    }

    private void DisplayAmmo()
    {
        int currentAmmo = ammoSlot.GetAmmoAmount(ammoType);
        ammoText.text = currentAmmo.ToString();
    }

    private IEnumerator Shoot()
    {
        if (ammoSlot.GetAmmoAmount(ammoType) > 0)
        {
            ammoSlot.ReduceAmmoAmount(ammoType);
            PlayMuzzleFlash();
            ProcessRayCast();
        }
    
        yield return new WaitForSeconds(shootDelay);
        _canShoot = true;
    }

    private void ProcessRayCast()
    {
        _canShoot = false;
        RaycastHit hit;
        Transform cameraTransform = fpsCamera.transform;
        Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range);

        if (hit.transform != null)
        {
            HitFlash(hit);
            
            EnemyHealth target = hit.transform.GetComponent<EnemyHealth>();

            if (target != null)
            {
                target.TakeDamage(damage);
            }
        }
    }

    private void HitFlash(RaycastHit hit)
    {
        GameObject impact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
        Destroy(impact, 0.1f);
    }
    
    private void PlayMuzzleFlash()
    {
        muzzleFlash.Play();
    }
}
